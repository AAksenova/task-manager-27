package ru.t1.aksenova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserOwnerModel extends AbstractModel {

    @Nullable
    private String userId;

}
