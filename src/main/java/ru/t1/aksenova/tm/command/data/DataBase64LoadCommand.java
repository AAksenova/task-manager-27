package ru.t1.aksenova.tm.command.data;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.Domain;
import ru.t1.aksenova.tm.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    public static final String DESCRIPTION = "Load data from base64 file.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final Path path = file.toPath();
        @NotNull final byte[] base64Byte = Files.readAllBytes(path);
        @Nullable final String base64Data = new String(base64Byte);
        @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Data);
        @Cleanup @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
